from dataclasses import dataclass, fields
import textwrap

@dataclass
class Ancestor:
    """Class for keeping track of an Ancestor."""
    name: str          = "Okänd"
    info: str          = ""
    alias: str         = "" 
    mother: 'Ancestor' = None  # Note: Forward reference to Ancestor class
    father: 'Ancestor' = None
    year_of_birth: int = 0
    date_of_birth: str = "DATE OF BIRTH"
    year_of_death: int = 0
    date_of_death: str = "DATE OF DEATH"
    place_of_birth:str = ""
    siblings: list     = None  # List of sibling Ancestor objects
    children: list     = None  # List of children Ancestor objects

    @property
    def short_name(self):
        return self.name.split()[0]

    @property
    def age(self) -> int:
        return 2023 - self.year_of_birth

    @property
    def parents(self) -> list:
        return list(filter(None, [self.mother,self.father]))
    
    def clone(self):
        # Use dataclasses.fields to get the fields of the dataclass
        field_values = {field.name: getattr(self, field.name) for field in fields(self)}
        return Ancestor(**field_values)
    
    def tree_structure(self, level=0, reverse = True) -> str:
        indent = "    " * level
        result = f"{indent}{self.name}"
        nodes = self.children if reverse else self.parents
        if nodes:
            result += "\n"
            for node in nodes:
                result += node.tree_structure(level + 1, reverse)
        return result

    def node_text(self) -> str:
        text =  f'{self.name}' 
        if not self.year_of_birth == 0: text += f' f. {self.year_of_birth}'
        if not self.place_of_birth == "": text += f' i {self.place_of_birth}'
        if not self.info == "": 
            text += f' {self.info}'
        return textwrap.fill(text,20)

    
    def __repr__(self) -> str:
        year_of_birth = f'\nf. {self.year_of_birth}' if not self.year_of_birth == 0 else ""
        place_of_birth = f'\ni {self.place_of_birth}' if not self.place_of_birth == "PLACE OF BIRTH" else ""
        text= f'{self.name} {year_of_birth}{place_of_birth}'
        return text

    def __str__(self) -> str:
        text = ""
        text +=f'{self.name} f. {self.year_of_birth}'
        return text
    
    def get_longest_name(self):
        val = len(self.name)
        if self.father:
            val = max(self.father.get_longest_name(),val)
        if self.mother:
            val = max(self.mother.get_longest_name(),val)
        return val
    
    def __hash__(self):
        # Use hash() on a tuple of the relevant attributes
        return hash((self.name, self.year_of_birth))

    def __eq__(self, other):
        # Check for equality based on the relevant attributes
        return (isinstance(other, Ancestor) and
                self.name == other.name and
                self.year_of_birth == other.year_of_birth)

GENERATIONS = []
def crawl(head: Ancestor, level = 0, code="0", reverse = False):
    global GENERATIONS
    if level == 0:
        GENERATIONS = []
    key = (code,(level,0),head)
    if level>len(GENERATIONS)-1:
        GENERATIONS.append([])
    GENERATIONS[level].append(key)
    nodes = head.children if reverse else head.parents
    if not nodes: return
    for i, node in enumerate(nodes):
        key = ((level,i),node)
        crawl(node,level+1,code = code +f'{i}',reverse=reverse)
    if not level == 0: return
    return GENERATIONS

if __name__ == "__main__":
    # Example usage:
    Lillian = Ancestor(name='Lillian', alias='Mormor')
    Georg   = Ancestor(name='Georg',   alias='Morfar')
    Irmelin = Ancestor(name='Irmelin', alias='Farmor')
    Sigvard = Ancestor(name='Sigvard', alias='Farfar')

    Louise  = Ancestor(name='Louise', year_of_birth=1967, mother=Lillian, father=Georg,  alias='Mamma')
    Claes   = Ancestor(name='Claes',  year_of_birth=1967, mother=Irmelin, father=Sigvard,alias='Pappa')

    David   = Ancestor(name='David',   year_of_birth=1997, mother=Louise, father=Claes, alias= 'Storebror')
    William = Ancestor(name='William', year_of_birth=2001, mother=Louise, father=Claes, alias= 'Mellanbror')
    Andre   = Ancestor(name='Andre',   year_of_birth=2003, mother=Louise, father=Claes, alias= 'Lillebror')

    # Set siblings for each individual
    David.siblings = [William, Andre]
    William.siblings = [David, Andre]
    Andre.siblings = [David, William]

    Georg.children   = [Louise]
    Lillian.children = [Louise]
    Sigvard.children = [Claes]
    Irmelin.children = [Claes]

    Louise.children = [David, William, Andre]
    Claes.children  = [David, William, Andre]

    crawl(David)
